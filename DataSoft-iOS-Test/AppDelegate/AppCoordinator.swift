//
//  AppCoordinator.swift
//  iOS-Test-DS
//
//  Created by Jamil Bin Hossain on 30/9/21.
//

import UIKit

final class AppCoordinator: Coordinator {
    
    
    var childCoordinators: [Coordinator] = []
    private let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
       print("AppCoordinator111")
        let tabBarCoordinator = TabBarCoordinator(window: self.window)
        tabBarCoordinator.start()
    }
    
    func popViewController() {
        
    }
    
//    func reStart(navigationController: UINavigationController) {
//
//        let loginCoordinator = LanguageCoordinator(navigationController: navigationController)
//        loginCoordinator.start()
//
//        window.rootViewController = navigationController
//        window.makeKeyAndVisible()
//    }
    
}
